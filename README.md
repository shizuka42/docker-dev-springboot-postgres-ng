# docker-dev-springboot-postgres-ng

Mise en place d'un environnement de développement sur un poste local avec des dockers (docker-compose), pour un projet web n-tiers avec Spring Boot, Postgreqsl et Angular

## Sommaire

- [docker-dev-springboot-postgres-ng](#docker-dev-springboot-postgres-ng)
  - [Sommaire](#sommaire)
  - [Objectif](#objectif)
  - [Contenu](#contenu)
  - [Mode d'emploi](#mode-demploi)
    - [Back : Note à propos du repository maven](#back--note-à-propos-du-repository-maven)
    - [Démarrer](#démarrer)
    - [Stopper](#stopper)
    - [Tester l'application back](#tester-lapplication-back)
    - [Tester l'application front](#tester-lapplication-front)
    - [Connexion à la base de données Postgresql](#connexion-à-la-base-de-données-postgresql)
    - [Ouvrir pg-admin](#ouvrir-pg-admin)
    - [Appliquer de nouveaux scripts Flyway](#appliquer-de-nouveaux-scripts-flyway)
    - [Lancer les tests automatiques Postman](#lancer-les-tests-automatiques-postman)
  - [Configuration des IDE/outils pour développer avec ces dockers](#configuration-des-ideoutils-pour-développer-avec-ces-dockers)
  - [Configurations des dockers](#configurations-des-dockers)
  - [Sources](#sources)

## Objectif

Avoir un docker-compose pour avoir un poste de développement pour un projet comprenant :

- front : angular
- back : spring boot + postman
- base de données : postgresql + flyway
- outils :
  - newman (postman en ligne de commande)
  - pg-admin

## Contenu

- container postgres pour la base de données
- container flyway pour initialiser la base postgres voisine grâce aux scripts de migration sql contenus dans `db/flyway/sql_migrations`
- container pg-admin pouvant se connecter à l'image docker postgres voisine
- container springboot : maven modifiée pour compiler l'application back avec maven, puis la démarrer
- container postman pour lancer automatiquement la collection de requêtes postman présente dans `back-app/postman` sur l'application back
- container ng pour l'application front

## Mode d'emploi

Pré-requis : docker et docker-compose doivent être installés

### Back : Note à propos du repository maven

L'image docker créée avec le `back-app/Dockerfile` est réalisée de façon à ne pas télécharger les dépendances maven à chaque build : le repository local de maven est conservé. Cependant, ceci n'est vrai tant que le fichier `back-app/app/pom.xml` n'a pas été modifié. Dès que ce fichier est modifié, toutes les dépendances seront retéléchargées.

### Démarrer

1. `docker-compose up --build -d`

Patienter quelques secondes le temps que tous les containers soint créés et qu'ils démarrent. Vous pouvez suivre leur démarrage dans les logs des containers.

### Stopper

1. `docker-compose down --volumes`

### Tester l'application back

1. Ouvrir dans un navigateur l'URL http://localhost:4210/hello ou http://localhost:4210/hello?name=shizuka42
1. ou `curl -v http://localhost:4210/hello?name=shizuka42`

### Tester l'application front

1. Ouvrir dans un navigateur l'URL http://localhost:4200/

### Connexion à la base de données Postgresql

La base de données `sample` est accessible via `localhost` sur le port `4220` avec le user/pwd `postgres/postgres`

### Ouvrir pg-admin

1. Ouvrir l'URL http://localhost:4240/ pour accèder à pg-admin
1. S'authentifier avec le user/password `pgadmin@shizuka42.fr/password`
1. Ouvrir la connexion déjà existante en donnant le password `postgres`

### Appliquer de nouveaux scripts Flyway

1. Ajouter ces scripts dans le dossier `db/flyway/sql_migrations` en respectant bien les normes Flyway
1. Relancer le container `flyway` via la commande `docker container start flyway` ou via l'UI de docker

### Lancer les tests automatiques Postman

Nota : Pour lancer les tests Postman, il faut que l'application ait fini de démarrer avec succès, c'est pour cela que la première exécution du docker au moment de sa création va probablement exécuter tous les tests en échec.

1. Relancer le container via la commande `docker container start postman` ou via le dashboard GUI de docker
1. Le résultat des tests est visible dans les logs du container, et est généré sous forme de rapport dans `postman/postman-report.xml`

## [Configuration des IDE/outils pour développer avec ces dockers](./docs/configuration-outils.md)

## [Configurations des dockers](./docs/configuration-dockers.md)

## Sources

- https://hub.docker.com/_/postgres
- https://hub.docker.com/r/dpage/pgadmin4
- https://www.pgadmin.org/docs/pgadmin4/latest/container_deployment.html
- https://hub.docker.com/r/flyway/flyway
- https://flywaydb.org/documentation/migrations#sql-based-migrations
- https://github.com/tomaszbartoszewski/postgresql-docker
- https://hub.docker.com/_/maven
- https://stackoverflow.com/questions/52120845/docker-compose-build-with-maven-that-re-uses-the-maven-repository
- https://hub.docker.com/_/node
- https://medium.elegantly.rocks/dockerize-your-angular-development-environment-3d5ea055ac2d
