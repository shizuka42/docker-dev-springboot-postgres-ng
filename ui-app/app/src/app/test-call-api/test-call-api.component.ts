import { HelloWorldService } from './../hello-world.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-test-call-api',
  templateUrl: './test-call-api.component.html',
  styleUrls: ['./test-call-api.component.less']
})
export class TestCallApiComponent implements OnInit {
  message:string;
  constructor(
    private helloWorldService : HelloWorldService,
  ) { }

  ngOnInit(): void {
    this.helloWorldService.getHelloWorld().subscribe(
      response => this.init(response),
    );
  }

  init(response): void {
    console.debug(response);
    this.message = response;
  }

}
