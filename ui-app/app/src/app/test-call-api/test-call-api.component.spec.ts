import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TestCallApiComponent } from './test-call-api.component';

describe('TestCallApiComponent', () => {
  let component: TestCallApiComponent;
  let fixture: ComponentFixture<TestCallApiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TestCallApiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestCallApiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
