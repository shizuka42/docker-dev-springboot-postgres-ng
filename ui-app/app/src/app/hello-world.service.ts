import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class HelloWorldService {
  constructor(
    private http: HttpClient
  ) { }

  getHelloWorld() {
    return this.http.get('http://localhost:4210/hello', {responseType: 'text'});
  }

}

