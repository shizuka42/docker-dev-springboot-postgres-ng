import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TestCallApiComponent } from './test-call-api/test-call-api.component';


const routes: Routes = [
  { path: '', component: TestCallApiComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
