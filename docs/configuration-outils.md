# Configuration des IDE/outils pour développer avec ces dockers

- [Configuration des IDE/outils pour développer avec ces dockers](#configuration-des-ideoutils-pour-développer-avec-ces-dockers)
  - [Spring-boot](#spring-boot)
    - [Build automatique](#build-automatique)
    - [Debug](#debug)
  - [Angular](#angular)
    - [Activer le debugger de Visual Studio Code sur une application NG déployée dans un docker](#activer-le-debugger-de-visual-studio-code-sur-une-application-ng-déployée-dans-un-docker)
      - [Configuration de Visual Studio Code](#configuration-de-visual-studio-code)
      - [Configuration de Chrome (sur Windows)](#configuration-de-chrome-sur-windows)
      - [Utiliser le debugger](#utiliser-le-debugger)
        - [Debug d'une page précise déjà ouverte](#debug-dune-page-précise-déjà-ouverte)
  - [Postman](#postman)

## Spring-boot

### Build automatique

Il faut activer le build automatique dans l'IDE pour que le docker voit les modifications de code et effectue un restart.

- Eclipse : Project > Build Automatically
- VS Code :
  - Aller dans File > Preferences > Settings
  - Rechercher "autobuild"
  - Activer la fonctionnalité "Java > Autobuild"

### Debug

Le mode remote debug est activé dans l'application spring-boot démarrée sur le docker.
Pour connecter votre IDE il faut utiliser les paramètres suivants :

- connexion : attach
- host : localhost
- port : 4215

## Angular

### Activer le debugger de Visual Studio Code sur une application NG déployée dans un docker

Le debugger de Visual Studio Code doit être configuré en mode remote pour fonctionner sur l'application déployée via docker, et votre Chrome doit être configuré pour démarrer avec le port de debug.

#### Configuration de Visual Studio Code

Il faut avoir installer le plugin "Debugger for Chrome" de Microsoft.

![plugin Debugger for Chrome de Microsoft](./resources/debug_docker_install_plugin.PNG "plugin Debugger for Chrome de Microsoft")

Pour cela il faut ajouter la configuration suivante dans votre `launch` :
```json
"launch": {
    "version": "0.2.0",
    "configurations": [
        {
        "type": "chrome",
        "request": "attach",
        "port": 9222,
        "name": "Chrome Docker Debug",
        "url": "http://localhost:4200/",
        "webRoot": "${workspaceFolder}"
        }
    ]
}
```

Pour cela vous pouvez utiliser le menu "debug" de Visual Studio Code et ajouter une nouvelle configuration (sur le projet, sur votre workspace ou en global, selon votre préférence) :

![Ajouter une configuration de debug dans VSC](./resources/debug_docker_menu_add_config.PNG "Ajouter une configuration de debug dans VSC")

#### Configuration de Chrome (sur Windows)

Pour lancer Chrome avec son mode debug, il faut lui ajoute à son exécution le paramètre suivant : `--remote-debugging-port=9222`

Pour simplifier cela, voilà comment faire un raccourci dédié à cela :
1. créer un nouveau raccourci vers Chrome (à partir de l'exe qui se trouve dans `C:\Program Files (x86)\Google\Chrome\Application`) et le nommer "Chrome Debug"
1. Ouvrir les propriétés du raccourci et modifier la "cible" pour lui ajouter le paramètre de debug, l'ouverture de l'application et le mode navigation privée pour ne pas lancer les plugins : `"C:\Program Files (x86)\Google\Chrome\Application\chrome.exe" --remote-debugging-port=9222 http://localhost:4200/ -incognito`
1. mettre ce raccourci là où vous pourrez facilement le lancer lorsque vous aurez besoin du debug

#### Utiliser le debugger

La solution la plus simple est la suivante :
1. Lancer le debugger dans Visual Studio Code avec la configuration "Chrome Docker Debug"   ![Lancer le debugger](./resources/debug_docker_menu_debugger.PNG "Lancer le debugger")
1. Ouvrir Chrome avec le raccourci "Chrome Debug" (pendant que le debugger de Visual Studio Code cherche la connexion)
1. Le debugger est alors connecté et actif

##### Debug d'une page précise déjà ouverte

Comme le debugger remote s'attache à Chrome par rapport à son URL exacte, vous ne pourrez pas lancer le debugger par défaut pour une page en cours dont l'url n'est pas exactement "http://localhost:4200/".

Si vous avez besoin de debugger une page précise déjà affichée par votre Chrome, il faut ajouter une nouvelle configuration de debug dans votre launch avec l'adresse précise souhaitée.

Par exemple :
* mon Chrome est actuellement sur la page http://localhost:4200/recherche et je veux la debugger sans recharger la page
* je vais ajouter une configuration dans mon launch :
```json
{
"type": "chrome",
"request": "attach",
"port": 9222,
"name": "Chrome Docker Debug TEMPORAIRE",
"url": "http://localhost:4200/recherche",
"webRoot": "${workspaceFolder}"
}
```
* je lance le debug sur Visual Studio Code avec cette configuration : le debug fonctionne

## Postman

Vous pouvez utiliser le client Postman sur votre poste de développement pour utiliser et compléter la collection de tests.

Pour cela vous devez :

- importer la collection de tests `postman/planet-api.postman_collection.json`
- l'environnement `postman/planet-api.postman_environnement.json` en surchargeant localement (dans les champs "current value") les variables pour interroger l'application déployée dans le docker :
  - base_url : localhost
  - port : 4210

Lorsque vous souhaitez ajouter une variable, il faut bien le faire dans l'environnement et pas directement dans la collection de tests (sinon ces variables ne seront pas applicables pour les tests automatiques).

Lorsque vous souhaitez compléter la collection de tests et/ou l'environnement, n'oubliez pas de les exporter dans le dossier `postman` pour les versionner avec git et les partager à tout le monde.