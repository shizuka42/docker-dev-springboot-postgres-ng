# Configurations des dockers

Utile pour comprendre comment adapter ce POC pour initialiser un nouveau projet.

## Postgresql

- `docker-compose.yml` dans la section du service `db` :
  - l'image docker actuellement utilisée est celle qui est tagée "alpine" sur [docker-hub](https://hub.docker.com/_/postgres) via le paramètre `image: postgres:alpine`
  - les variables d'environnement dans le paramètre `environment` permettent de définir une base de données, un user et son mot de passe. Ces variables sont détaillées dans [la description de l'image docker postgres](https://hub.docker.com/_/postgres)
  - le port localhost qui permet d'interroger la base en dehors du container est défini par le paramètre `ports: - 4220:5432`

## pg-Admin

- `docker-compose.yml` dans la section du service `pg-admin` :
  - l'image docker actuellement utilisée est celle qui est tagée "latest" sur [docker-hub](https://hub.docker.com/r/dpage/pgadmin4) via le paramètre `image: dpage/pgadmin4:latest`
  - les variables d'environnement dans le paramètre `environment` permettent de définir une base de données, un user et son mot de passe. Ces variables sont détaillées dans [la description de l'image docker pgadmin](https://www.pgadmin.org/docs/pgadmin4/latest/container_deployment.html)
  - le port localhost qui permet d'accèder à l'interface web de pgadmin en dehors du container est défini par le paramètre `ports: - 4240:80`
  - le volume défini dans le paramètre `volumes` permet de partager le fichier `db/pg-admin/docker-servers.json` avec le container
- `db/pg-admin/docker-servers.json` : fichier de configuration initiale des connexions enregistrées dans pg-admin. Cela permet d'avoir directement le lien vers la base de données postgres dans la liste des connexions.

## Flyway

- `docker-compose.yml` dans la section du service `flyway` :
  - l'image docker actuellement utilisée est celle qui est tagée "latest-alpine" sur [docker-hub](https://hub.docker.com/r/flyway/flyway) via le paramètre `image: flyway/flyway:latest-alpine`
  - le volumes définis dans le paramètre `volumes` permettent de partager le fichier `db/flyway/docker-flyway.config` et le répertoire `db/flyway/sql_migrations` avec le container
  - le paramètre `command` définit la commande effectuée au lancement du container
- `flyway/docker-flyway.config`: fichier de configuration de flyway pour sa connexion vers la base de données postgres
- `flyway/sql_migrations` : répertoire contenant les scripts sql Flyway

## Maven

- Pour choisir la version de Maven utilisée, il faut changer le tag de l'image de base dans le `back-app/Dockerfile` : `FROM maven:3.6-openjdk-11`
- Pour exclure des fichiers du docker (qu'il ne soit pas copiés dans le docker), il faut ajouter les patterns dans le fichier `back-app/.dockerignore`
- Pour préciser le répertoire contenant les sources sur votre PC, il faut changer les lignes suivantes :
  - dans `back-app/DockerFile` :
    - changer `ADD ./app/pom.xml /app` vers `ADD PATH_SRC_ON_COMPUTER/pom.xml /app`
    - changer `COPY ./app /app` vers `COPY PATH_SRC_ON_COMPUTER /app`

## Sprinboot

- Pour démarrer l'application, il faut précisément nommer le nom du package jar dans le `back-app/Dockerfile` (nom défini à partir du `finalname` dans le `back-app/app/pom.xml`) : `ENTRYPOINT [ "java", "-jar", "/app/target/demo.jar"]`
- le port localhost qui permet d'interroger l'application en dehors du container est défini par le paramètre `ports: - 4210:8080` du service `springboot` dans `docker-compose.yml`

## Node

- Pour choisir la version de node.js utilisée, il faut changer le tag de l'image de base dans le `ui-app/Dockerfile` : (ici il s'agit de la 12-alpine) `FROM node:12-alpine`

## Angular

- Pour choisir la version de Angular utilisée, il faut changer la version récupérée dans le `ui-app/Dockerfile` : (ici il s'agit de la dernière version compatible avec la 9.1.8) `RUN npm install -g @angular/cli@~9.1.8`
- Pour exclure des fichiers du docker (qu'il ne soit pas copiés dans le docker), il faut ajouter les patterns dans le fichier `ui-app/.dockerignore`
- Pour préciser le répertoire contenant les sources sur votre PC, il faut changer les lignes suivantes :
  - dans `ui-app/DockerFile` :
    - changer `COPY ./app/package.json /app/package.json` vers `COPY PATH_SRC_ON_COMPUTER/package.json /app/package.json`
    - changer `COPY ./app /app` vers `COPY PATH_SRC_ON_COMPUTER /app`
  - dans `docker-compose.yml` :
    - changer `volumes: - ./app:/app` vers `volumes: - PATH_SRC_ON_COMPUTER:/app`
- le port localhost qui permet d'interroger l'application en dehors du container est défini par le paramètre `ports: - 4200:4200` du service `ng-development` dans `docker-compose.yml`