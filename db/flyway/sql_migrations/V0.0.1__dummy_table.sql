-- CREATE DUMMY TABLE
CREATE TABLE public.dummy
(
    id BIGSERIAL NOT NULL,
    name VARCHAR(255) NOT NULL,
    date DATE NOT NULL
);
ALTER TABLE public.dummy
ADD CONSTRAINT pk_dummy_id PRIMARY KEY (id);

-- INSERT SOME DATA
INSERT INTO public.dummy
    (name, date)
VALUES
    ('Dummy #1', now()),
    ('Dummy #2', now()),
    ('Dummy #3', now()),
    ('Dummy #4', now()),
    ('Dummy #5', now());