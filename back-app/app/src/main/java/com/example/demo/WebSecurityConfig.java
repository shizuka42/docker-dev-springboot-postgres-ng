package com.example.demo;

import org.springframework.boot.actuate.autoconfigure.security.servlet.EndpointRequest;
import org.springframework.boot.autoconfigure.security.servlet.PathRequest;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        // limitation de l'accès au endpoint de actuator aux utilisateurs de
        // spring.security ayant
        // le profil "ENDPOINT_ADMIN" (en cohérence avec les propriétés dans
        // application.yml)
        http.authorizeRequests().requestMatchers(EndpointRequest.toAnyEndpoint()).hasRole("ENDPOINT_ADMIN")
                .requestMatchers(PathRequest.toStaticResources().atCommonLocations()).authenticated().and().httpBasic();
    }
}
